package kunanets.tutors.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PublicController {


    @RequestMapping("/")
    public String home() {
        return "home.html";
    }

}